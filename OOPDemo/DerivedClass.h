//
//  DerivedClass.h
//  OOPDemo
//
//  Created by James Cash on 30-09-15.
//  Copyright © 2015 Occasionally Cogent. All rights reserved.
//

#import "BaseClass.h"

@interface DerivedClass : BaseClass

@end
