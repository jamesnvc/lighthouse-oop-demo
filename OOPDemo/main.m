//
//  main.m
//  OOPDemo
//
//  Created by James Cash on 30-09-15.
//  Copyright © 2015 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>
#import "BaseClass.h"
#import "DerivedClass.h"

int main(int argc, const char * argv[]) {
    @autoreleasepool {
        BaseClass *foo = [[BaseClass alloc] initWithMultiplier:2];
        [foo frobnobicateWidget:5];
        NSLog(@"%@",[foo examine:[[NSObject alloc] init]]);
        NSLog(@"%@",[foo examine:foo]);
        NSLog(@"%@",[foo examine:nil]);

        NSLog(@"%f", [foo floatMult:@(5.5)]);
        NSLog(@"%f", [foo floatMult:nil]);


        foo.multiplier = 3;
        [foo frobnobicateWidget:5];

        DerivedClass *bar = [[DerivedClass alloc] init];
        [bar frobnobicateWidget:8];

        bar.multiplier = 10;
        [bar frobnobicateWidget:8];

        DerivedClass *quux = [[DerivedClass alloc] initWithMultiplier:2];
        [quux frobnobicateWidget:5];
    }
    return 0;
}
