//
//  BaseClass.m
//  OOPDemo
//
//  Created by James Cash on 30-09-15.
//  Copyright © 2015 Occasionally Cogent. All rights reserved.
//

#import "BaseClass.h"

@implementation BaseClass

- (instancetype)init
{
    self = [super init];
    if (self) {
        self.multiplier = 1;
    }
    return self;
}

- (instancetype)initWithMultiplier:(NSInteger)multi
{
    self = [super init];
    if (self) {
        self.multiplier = multi;
    }
    return self;
}

- (void)frobnobicateWidget:(NSInteger)widgetId
{
    NSLog(@"widgetId = %ld", widgetId * self.multiplier);
}

- (NSString*)examine:(NSObject*)thing
{
    return [thing description];
}

- (CGFloat)floatMult:(NSNumber *)num
{
    if (num) {
        return self.multiplier * [num floatValue];
    } else {
        return (CGFloat)self.multiplier;
    }
}

@end
