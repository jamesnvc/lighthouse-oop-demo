//
//  BaseClass.h
//  OOPDemo
//
//  Created by James Cash on 30-09-15.
//  Copyright © 2015 Occasionally Cogent. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface BaseClass : NSObject

@property (nonatomic,assign) NSInteger multiplier;

- (instancetype)initWithMultiplier:(NSInteger)multi;
- (void)frobnobicateWidget:(NSInteger)widgetId;
- (NSString*)examine:(NSObject*)thing;
- (CGFloat)floatMult:(NSNumber*)num;

@end
