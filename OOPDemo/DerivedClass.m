//
//  DerivedClass.m
//  OOPDemo
//
//  Created by James Cash on 30-09-15.
//  Copyright © 2015 Occasionally Cogent. All rights reserved.
//

#import "DerivedClass.h"

@implementation DerivedClass

- (instancetype)initWithMultiplier:(NSInteger)multi
{
    self = [super initWithMultiplier:multi*2];
    return self;
}

- (void)frobnobicateWidget:(NSInteger)widgetId
{
    NSLog(@"getting ready to frobnobicate with %ld", self.multiplier);
    [super frobnobicateWidget:widgetId*2];
    NSLog(@"frobnobicated");
}

@end
